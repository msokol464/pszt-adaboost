import numpy as np
import copy as cp
from src import Utils
from src import DecisionStump


class AdaBoost:

    def __init__(self, num_of_learners, num_of_classes=2):

        self.num_of_learners = num_of_learners
        self.num_of_classes = num_of_classes
        self.weak_learner_list = []
        self.weak_learner_weights = []
        self.alpha = None
        self.num_of_features = None

    def train(self, dataset, labels):
        """
        Method training classifier on dataset and labels.

        Parameters:
        dataset             --  numpy array with data
        labels              --  numpy array with targets

        Returns:
        error               --  training error
        """
        num_of_points = len(dataset)
        self.num_of_features = len(dataset[0])
        assert num_of_points == len(labels)

        weights = np.array([1/num_of_points for i in range(num_of_points)])

        for _ in range(self.num_of_learners):

            h = DecisionStump.DecisionStump(100)
            err = h.train(dataset, labels, weights)
            predictions = h.make_prediction(dataset)
            alpha_t = 0.5*np.log((1-err)/err)
            self.weak_learner_weights.append(alpha_t)

            for i in range(num_of_points):
                weights[i] = weights[i] * \
                    np.exp(-alpha_t*predictions[i]*labels[i])/weights.sum()

            self.weak_learner_list.append(h)

        predictions = self.make_prediction(dataset)
        error = Utils.compute_error(predictions, labels)
        return error

    def make_prediction(self, dataset):
        """
        Method predicting results from dataset.

        Parameters:
        dataset             --  numpy array with data

        Returns:
        results             --  numpy array with prediction results
        """
        num_of_points = len(dataset)
        num_of_features = len(dataset[0])
        assert num_of_features == self.num_of_features
        result = np.zeros(num_of_points)

        for i, h in enumerate(self.weak_learner_list):
            predictions = h.make_prediction(dataset)
            for j, p in enumerate(predictions):
                result[j] += p*self.weak_learner_weights[i]

        result = np.sign(result)
        return result

    def training_report(self):
        """
        Method displaying training report with most valuable data about every learner.
        """
        headers = ['age', 'sex', 'restbp', 'chol', 'fbs', 'thalach', 'exang', 'oldpeak',
                   'cp t1', 'cp t2', 'cp t3', 'cp t4', 'restecg norm', 'rescedg abn', 'restecg hyper']
        for i, wl in enumerate(self.weak_learner_list):
            print('-----Learner {} details-----'.format(i))
            print('feature: {}\ndivision: {:.5f}\ndirection: {}\nerror: {:.5f}'.format(
                headers[wl.best_feature],
                # wl.best_feature,
                wl.best_division,
                wl.best_direction,
                self.weak_learner_weights[i]
            ))
