import os
import time
import pandas as pd
import numpy as np
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split, cross_val_score
import config
from src import AdaBoost
from figures import hd_vs_age


def process_datasets():
    """
    Function processing raw data and saving them in directory with processed files.
    """
    for filename in os.listdir(config.data_raw_dir):
        if filename not in os.listdir(config.data_processed_dir):
            df = pd.read_csv(config.data_raw_dir + filename, header=None)
            df.columns = ['age', 'sex', 'cp', 'restbp', 'chol', 'fbs', 'restecg',
                          'thalach', 'exang', 'oldpeak', 'slope', 'ca', 'thal', 'class']
            df.drop(['ca'], axis=1, inplace=True)
            df.drop(['thal'], axis=1, inplace=True)
            df.drop(['slope'], axis=1, inplace=True)
            clean_df = df.loc[(df['age'] != '?')
                              & (df['restbp'] != '?')
                              & (df['chol'] != '?')
                              & (df['fbs'] != '?') & ('-' not in df['fbs'])
                              & (df['restecg'] != '?') & ('-' not in df['restecg'])
                              & (df['exang'] != '?') & ('-' not in df['exang'])
                              & (df['oldpeak'] != '?') & ('-' not in df['oldpeak'])]
            clean_df.to_csv(config.data_processed_dir + filename, index=False)


def open_and_adjust_dataset(file_path):
    """
    Function loading dataset from file and adjusting the data.

    Parameters:
    file_path           --  path to file

    Returns:
    X                   -- numpy array with data
    Y                   -- numpy array with targets  
    """
    df = pd.read_csv(file_path, header=0)
    dataset = df.iloc[:, :-1]
    # print(dataset)
    labels = df['class'].copy()
    dataset = pd.get_dummies(dataset, columns=['cp', 'restecg'])
    # print(dataset)
    indexes = labels > 0
    labels[indexes] = 1
    indexes = labels <= 0
    labels[indexes] = -1
    X = dataset.values
    Y = labels.values
    return X, Y


def get_dataset():
    """
    Function collecting whole dataset from files in directory from config.py file.

    Returns:
    data_with_targets      -- numpy array with data and targets  
    """
    file_names = os.listdir(config.data_processed_dir)

    data, targets = open_and_adjust_dataset(
        config.data_processed_dir + file_names[0])
    for file_name in file_names[1:]:
        tmp_data, tmp_targets = open_and_adjust_dataset(
            config.data_processed_dir + file_name)
        data = np.concatenate((data, tmp_data))
        targets = np.concatenate((targets, tmp_targets))

    data_with_targets = np.insert(data, data.shape[1], targets, axis=1)

    return data_with_targets


def split_dataset_for_classic_training(data_with_targets, test_procent, seed=None):
    """
    Function splitting data for training and test based on test_procent value.

    Parameters:
    data_with_targets   -- numpy array with data and targets
    test_percent        -- percent of data for test set
    seed                -- number for random seed for data shuffle

    Returns:
    training_set        -- set for training
    test_set            -- set for test
    training_targets    -- targets for training
    test_targets        -- targets for test 
    """
    np.random.seed(seed)

    np.random.shuffle(data_with_targets)

    split_index = int((1 - test_procent) * len(data_with_targets))

    training_set = data_with_targets[: split_index, : -1]
    test_set = data_with_targets[split_index:, : -1]
    training_targets = data_with_targets[: split_index, -1:].ravel()
    test_targets = data_with_targets[split_index:, -1:].ravel()

    return training_set, test_set, training_targets, test_targets


def split_dataset_for_kfold_validation(data_with_targets, number_of_pieces):
    """
    Function splitting dataset for KFold validation.

    Parameters:
    data_with_targets   --  numpy array with data and targets
    number_of_pieces    --  number of dataset slices == K

    Returns:
    datasets            --  list of numpy arrays reresenting sliced data_with_targets
    """
    split_indexes = [int(len(data_with_targets)/number_of_pieces)
                     * index for index in range(number_of_pieces + 1)]
    datasets = []
    for index in range(len(split_indexes) - 1):
        dataset_part = data_with_targets[split_indexes[index]: split_indexes[index + 1], :]
        datasets.append(dataset_part)

    return datasets


def run_implemented_adaboost(number_of_estimators, test_percent, random_seed):
    """
    Function running implemented AdaBoost Classifier.

    Parameters:
    number_of_estimators    --  number of estimators
    test_percent            --  float number representing percent of test data
    random_seed             --  number for random seed
    """
    process_datasets()

    data_with_targets = get_dataset()

    print('Training set and test set reading...')
    training_data, test_data, training_targets, test_targets = split_dataset_for_classic_training(
        data_with_targets, test_percent, random_seed)

    start_time = time.time()

    ada = AdaBoost.AdaBoost(number_of_estimators)

    print('------TRAINING------')
    err = ada.train(training_data, training_targets)
    
    end_time = time.time()
    elapsed_time = end_time - start_time

    print('--------TRAINING TIME--------')
    print(f'Training time: {elapsed_time:.5f}s')

    print(f'Error: {err:.5f}')
    # print('Best feature: {}'.format(stump.best_feature))
    # print('Best division: {}'.format(stump.best_division))
    # print('Best direction: {}'.format(stump.best_direction))

    # ada.training_report()

    print('-----PREDICTION-----')
    predictions = ada.make_prediction(test_data)
    prediction_error = compute_error(predictions, test_targets)
    print(f'Prediction success: {(1 - prediction_error):.5f}')
    print(f'Prediction error: {prediction_error:.5f}')

    # print('Predictions')
    # print(predictions)
    # print('Actual results')
    # print(test_targets)

    hd_vs_age.drawFigure(test_data, predictions, test_targets)

    print('-----VALIDATION-----')
    validation_datasets = split_dataset_for_kfold_validation(
        data_with_targets, config.k_fold)

    scores = []
    for index in range(config.k_fold):
        test_dataset = validation_datasets[index]
        training_datasets = tuple(validation_datasets[dataset_index] for dataset_index in range(
            len(validation_datasets)) if dataset_index != index)
        training_dataset = np.concatenate(training_datasets)

        test_data = test_dataset[:, : -1]
        test_targets = test_dataset[:, -1:].ravel()

        training_data = training_dataset[:, : -1]
        training_targets = training_dataset[:, -1:].ravel()

        ada.train(training_data, training_targets)
        predictions = ada.make_prediction(test_data)
        prediction_error = compute_error(predictions, test_targets)
        scores.append(1 - prediction_error)

    average_score = sum(scores)/len(scores)
    print('------CROSS-VALIDATION------')
    print(f'Validation accuracy: {average_score:.5f}')
    print(f"Validation error: {1 - average_score:.5f}")


def run_sci_kit_adaboost(number_of_estimators, test_percent, random_seed, depth_of_tree):
    """
    Function running Sci-Kit AdaBoost Classifier.

    Parameters:
    number_of_estimators    --  number of estimators
    test_percent            --  float number representing percent of test data
    random_seed             --  number for random seed
    depth_of_tree           --  max_depth of sci-kit DecisionTreeClassfier
    """
    process_datasets()

    data_with_targets = get_dataset()

    training_set, test_set, training_targets, test_targets = split_dataset_for_classic_training(
        data_with_targets, test_percent, random_seed)

    start_time = time.time()

    classifierAdaBoost = AdaBoostClassifier(
        DecisionTreeClassifier(
            max_depth=depth_of_tree
        ),
        n_estimators=number_of_estimators
    )

    print('------TRAINING------')
    classifierAdaBoost.fit(training_set, training_targets)

    end_time = time.time()
    elapsed_time = end_time - start_time

    print('--------TRAINING TIME--------')
    print(f'Training time: {elapsed_time:.5f}s')

    print('-----PREDICTION-----')
    predictions = classifierAdaBoost.predict(test_set)
    print(
        f'AdaBoost classic training/test accuracy error: {1 - accuracy_score(test_targets, predictions):.5f}')

    hd_vs_age.drawFigure(test_set, predictions, test_targets)

    dataset = np.concatenate((training_set, test_set))
    targets = np.concatenate((training_targets, test_targets))

    validation = cross_val_score(classifierAdaBoost, dataset, targets)
    average_score = sum(validation)/len(validation)
    print('------CROSS-VALIDATION------')
    print(f"Validation accuracy: {average_score:.5f}")
    print(f"Validation error: {1 - average_score:.5f}")


def compute_error(predictions, labels):
    """
    Function computing error between predicted values and real values from dataset.

    Parameters:
    predictions         --  list of predicted values
    labels              --  list of real values

    Returns:
    err/num_of_points   --  computed error
    """
    num_of_points = len(predictions)
    assert num_of_points == len(labels)

    err = 0
    for p in range(num_of_points):
        if predictions[p] != labels[p]:
            err += 1

    return err/num_of_points
