import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


class Node:

    def __init__(self, depth, div=None, dir=None, feature=None, is_leaf=False, value=None, left=None, right=None):
        self.depth = depth
        self.div = div
        self.dir = dir
        self.feature = feature
        self.is_leaf = is_leaf
        self.value = value
        self.left = left
        self.right = right


class Tree:

    def __init__(self, max_d, steps):
        self.max_depth = max_d
        self.num_of_features = None
        self.root = None
        self.steps = steps

    def optimize(self, training_set, feature, labels, weights, steps):

        num_of_points = training_set.shape[0]
        feature_col = np.array([item[feature] for item in training_set])
        col_values = np.uniqe(feature_col)
        if col_values.shape[0] > 2:
            start_val = np.minimum(col_values)
            end_val = np.maximum(col_values)
            feature_range = end_val - start_val
        else:
            steps = 1
            start_val = col_values[0]
            end_val = col_values[1]
            feature_range = 1

        min_err = sum(weights)
        best_direction = 1
        best_division = start_val

        if steps > 0 and feature_range > 0:
            step_size = feature_range/steps
            div = start_val
            while div < end_val:
                for dir in [1, -1]:
                    predictions = np.ones(num_of_points)
                    for j, p in enumerate(feature_col):
                        if p < div:
                            predictions[j] = dir
                        else:
                            predictions[j] = -dir
                    err = np.sum(weights*(predictions != labels))
                    if err < min_err:
                        min_err = err
                        best_direction = dir
                        best_division = div
                div += step_size

            return min_err, best_direction, best_division

    def make_tree(self, dataset, labels, depth, weights):
        num_of_points = dataset.shape[0]
        unique_labels = len(np.unique(labels))

        if(num_of_points == 0):
            raise "No data, can't create Node"
        if(unique_labels == 1):
            return Node(depth=depth, is_leaf=True, value=labels[0])
        if(depth >= self.max_depth):
            counts = np.bincount(labels)
            return Node(depth=depth, is_leaf=True, value=np.argmax(counts))

        best_feature = 0
        best_direction = 1
        best_division = 0
        min_err = np.sum(weights)

        for f in range(self.num_of_features):
            err, direction, division = self.optimize(
                dataset, f, labels, weights, self.steps)
            if err < min_err:
                min_err = err
                best_feature = f
                best_division = division
                best_direction = direction

        part1 = np.array([dataset[i, :] for i in range(
            dataset.shape[0]) if dataset[i, best_feature]*best_direction > best_division*best_direction])
        labels1 = np.array([labels[i] for i in range(
            dataset.shape[0]) if dataset[i, best_feature]*best_direction > best_division*best_direction])
        part2 = np.array([dataset[i, :] for i in range(
            dataset.shape[0]) if dataset[i, best_feature]*best_direction <= best_division*best_direction])
        labels2 = np.array([labels[i] for i in range(
            dataset.shape[0]) if dataset[i, best_feature]*best_direction <= best_division*best_direction])
        right = self.make_tree(part1, labels1, depth+1, weights)
        left = self.make_tree(part2, labels2, depth+1, weights)
        split_node = Node(depth, best_division, best_direction,
                          best_feature, False, None, left, right)
        return split_node

    def train(self, dataset, labels, weights):
        self.num_of_features = dataset.shape[1]
        assert len(weights) == dataset.shape[0]
        self.root = self.make_tree(dataset, labels, 0, weights)
