class DecisionStump:

    def __init__(self, steps):
        self.steps = steps
        self.num_of_features = None
        self.best_feature = None
        self.best_division = None
        self.best_direction = None
        self.num_of_points = None

    def train(self, training_set, labels, weights):
        """
        Parameters:
        training_set        --  numpy array with data
        labels              --  numpy array with targets
        weights             --  weights of datapoints

        Returns:
        min_err             --  weighed error on trainig data classification
        """
        num_of_points = len(training_set)
        self.num_of_features = len(training_set[0])
        assert num_of_points == len(labels)

        best_feature = 0
        best_direction = 1
        best_division = 0
        min_err = sum(weights)
        for f in range(self.num_of_features):
            err, direction, division = self.optimize(
                training_set, f, labels, weights, self.steps)
            if err < min_err:
                min_err = err
                best_feature = f
                best_division = division
                best_direction = direction

        self.best_feature = best_feature
        self.best_division = best_division
        self.best_direction = best_direction

        return min_err

    def make_prediction(self, data_set):
        """
        Method making predictions based on data_set.

        Parameters:
        data_set            --  numpy array with data

        Returns:
        predictions         --  numpy array with prediction
        """
        num_of_points = len(data_set)
        num_of_features = len(data_set[0])
        assert self.num_of_features == num_of_features

        predictions = [1 for i in range(num_of_points)]
        for p in range(num_of_points):
            if data_set[p][self.best_feature] < self.best_division:
                predictions[p] = self.best_direction
            else:
                predictions[p] = (-1)*self.best_direction
        return predictions

    def optimize(self, training_set, feature, labels, weights, steps):
        """


        Parameters:
        training_set        --  numpy array with data
        feature             --  selected feature from dataset
        labels              --  actual datalabels
        weights             --  numpy array of datapoints weights
        steps               --  maximum number of steps


        Returns:
        min_err             --  weighed error on training data
        best_direction      --  dividing direction
        best_division       --  threshold value for splitting datapoints
        """
        num_of_points = len(training_set)
        feature_col = [item[feature] for item in training_set]
        start_val = min(feature_col)
        end_val = max(feature_col)
        feature_range = end_val - start_val

        min_err = sum(weights)
        best_direction = 1
        best_division = start_val

        if steps > 0 and feature_range > 0:
            step_size = feature_range/steps
            div = start_val
            while div <= end_val:
                for dir in [1, -1]:
                    predictions = [1 for i in range(num_of_points)]
                    for j, p in enumerate(feature_col):
                        if p < div:
                            predictions[j] = dir
                        else:
                            predictions[j] = -dir
                    err = sum(weights[x] for x in range(
                        num_of_points) if predictions[x] != labels[x])
                    if err < min_err:
                        min_err = err
                        best_direction = dir
                        best_division = div
                div += step_size
            return min_err, best_direction, best_division
