import matplotlib.pyplot as plt
import numpy as np

def drawFigure(dataset, predictions, labels):
    plt.subplots(figsize =(8,5))
    intervals = ['<=20', '21–30', '31–40', '41–50', '51–60', '61 and Above']
    thresholds = [20, 30, 40, 50, 60, 120]
    hd = [0, 0, 0, 0, 0, 0]
    predicted_hd = [0, 0, 0, 0, 0, 0]
    sorted_by_age = dataset[np.argsort(dataset[:, 0])]

    range = 0
    for i, p in enumerate(sorted_by_age):
        if p[0] <= thresholds[range]:
            if labels[i] == 1 : hd[range] += 1
            if predictions[i] == 1 : predicted_hd[range] += 1
        else :
            range += 1
            if labels[i] == 1 : hd[range] += 1
            if predictions[i] == 1 : predicted_hd[range] += 1


    line1 = plt.plot(intervals, hd , color='r', marker='o', linestyle ='dashed', markerfacecolor='y', markersize=10)
    line2 = plt.plot(intervals, predicted_hd, color='b',marker='o', linestyle ='dashed', markerfacecolor='y', markersize=10 )
    plt.xlabel('Age')
    plt.ylabel('Number of patients')
    plt.title('Heart disease in age ranges')
    plt.legend((line1[0], line2[0]), ('Heart disease', 'Predicted heart disease'))
    plt.show()  

def drawHeatmap():
    pass
