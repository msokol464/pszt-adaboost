import argparse
import numpy as np
import pandas as pd
from src import Utils
import config

if __name__ == '__main__':
    argument_parser = argparse.ArgumentParser()
    argument_parser.add_argument(
        "-s",
        "--scikit",
        help="Specify if you want to run Sci-Kit learn Adaboost.",
        action="store_true"
    )
    argument_parser.add_argument(
        "-e",
        "--estimators",
        help="Specify number of estimators.",
        action="store",
        type=int,
        default=10
    )
    argument_parser.add_argument(
        "-t",
        "--test_percent",
        help="Specify how many percent of dataset should be used as test.",
        action="store",
        type=int,
        choices=range(10, 90),
        default=20
    )
    argument_parser.add_argument(
        "-r",
        "--random_seed",
        help="Specify number for random seed.",
        action="store",
        type=int,
        default=None
    )
    argument_parser.add_argument(
        "-d",
        "--depth_of_tree",
        help="Specify depth of decision tree. Only for Sci-Kit Adaboost.",
        action="store",
        type=int,
        default=1
    )
    args = argument_parser.parse_args()
    if args.scikit:
        Utils.run_sci_kit_adaboost(
            args.estimators, args.test_percent/100, args.random_seed, args.depth_of_tree)
    else:
        Utils.run_implemented_adaboost(
            args.estimators, args.test_percent/100, args.random_seed)

