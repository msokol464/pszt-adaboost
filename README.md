# PSZT AdaBoost

## Treść zadania
Zaimplementować algorytm AdaBoost (Adaptive Boosting) do klasyfikacji bazujący na sekwencji drzew decyzyjnych
Zbiór danych do użycia: Heart Disease - [archive.ics.uci.edu](https://archive.ics.uci.edu/ml/datasets/Heart+Disease)
Uzyskane rezultaty porównać z wynikami dla wybranej implementacji algorytmu ML z dostępnych bibliotek 
np. Scikit-learn, WEKA, MLlib, Tensorflow/Keras etc 

## Struktura projektu

	pszt-adaboost/
	├── data/
	│   └── raw/
	│   └── processed/
	├── figures/
	├── src/
	│   └── __init__.py
	│   └── DecisionStump.py 
	│   └── AdaBoost.py 
	│   └── Utils.py 
	├── tests/
	│   └── __init__.py
	├── README.md
	└── main.py 

## Uruchamianie

Aby uzyskać informacje na temat argumentów skryptu uruchom komendę:
```
python main.py -h
```
